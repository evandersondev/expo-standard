# Template expo with standard lintter

---

### packages pre-configurate
- eslint
- prettier
- env
- editorConfig
- expo-fonts

### packges additionals
- @expo-google-fonts/roboto
- react navigation
- expo-linking
- styled-components
- expo-vector-icons

module.exports = function (api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'babel-plugin-root-import',
        {
          rootPathSuffix: 'src',
        },
      ],
      [
        'inline-dotenv',
        {
          unsafe: true,
        },
      ],
    ],
    env: {
      production: {
        plugins: [
          'babel-plugin-root-import',
          {
            rootPathSuffix: 'src',
          },
        ],
      },
    },
  };
};

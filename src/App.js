import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import { AppLoading } from 'expo';
// import { Roboto_400Regular, useFonts } from '@expo-google-fonts/roboto';

export default function App() {
  // const [fontsLoaded] = useFonts({
  //   Roboto_400Regular,
  // });

  // if (!fontsLoaded) {
  //   return <AppLoading />;
  // }

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Template expo with Standard</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#212129',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 24,
    color: '#ffffff',
  },
});
